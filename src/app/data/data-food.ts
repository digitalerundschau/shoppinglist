import { Food } from '../interfaces/food';

export const FRUITS: Food[] = [
  { pc: 1, name: 'Bananen' },
  { pc: 1, name: 'Erdbeeren' },
  { pc: 1, name: 'Weintrauben' },
  { pc: 1, name: 'Äpfel' },
  { pc: 1, name: 'Pfirsiche' },
  { pc: 1, name: 'Himbeeren' }
];

export const VEGETABLES: Food[] = [
  { pc: 1, name: 'Tomaten' },
  { pc: 1, name: 'Gurken' },
  { pc: 1, name: 'Kartoffeln' },
  { pc: 1, name: 'Paprika' },
  { pc: 1, name: 'Broccoli' },
  { pc: 1, name: 'Möhren' }
];

export const BREADS: Food[] = [
  { pc: 1, name: 'Landbrot' },
  { pc: 1, name: 'Toastbrot' },
  { pc: 1, name: 'Brötchen' },
  { pc: 1, name: 'Tiefkühlbrötchen' }
];

export const LUNCHMEAT: Food[] = [
  { pc: 1, name: 'Käse' },
  { pc: 1, name: 'Wurst' },
  { pc: 1, name: 'Streichkäse' },
  { pc: 1, name: 'Fleischsalat' },
  { pc: 1, name: 'Marmelade' }
];

export const BEVERAGES: Food[] = [
  { pc: 1, name: 'Selter' },
  { pc: 1, name: 'Wasser' },
  { pc: 1, name: 'Saft' },
  { pc: 1, name: 'Bier' }
];
