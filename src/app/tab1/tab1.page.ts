import {Component, OnInit} from '@angular/core';
import {BEVERAGES, BREADS, FRUITS, LUNCHMEAT, VEGETABLES} from '../data/data-food';
import {Food} from '../interfaces/food';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  fruits = FRUITS;
  vegetables = VEGETABLES;
  breads = BREADS;
  lunchmeat = LUNCHMEAT;
  beverages = BEVERAGES;

  selectedFood?: Food;

  constructor() {}

  ngOnInit(): void {
  }

  saveShoppingList() {
    console.log('Test');
  }

}
