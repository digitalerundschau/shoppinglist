import {Component, OnInit} from '@angular/core';
import {FoodService} from '../services/food.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {
  selectedFood?: any = [];

  constructor(public foodService: FoodService) {}

  ngOnInit(): void {
    this.selectedFood = this.foodService.getData();
  }

  deleteSelectedFood() {
    this.foodService.removeData();
  }

  getData() {
    this.selectedFood = this.foodService.getData();
  }
}
