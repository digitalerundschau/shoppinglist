import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FoodService {
  selectedForBuy: any = [];

  constructor() { }

  public saveData() {
    localStorage.setItem('selectedForBuy', this.selectedForBuy);
  }

  public getData() {
    return localStorage.getItem('selectedForBuy');
  }

  public removeData() {
    localStorage.removeItem('selectedForBuy');
  }
}
