import {Component, Input, OnInit} from '@angular/core';
import {Food} from '../../interfaces/food';
import {FoodService} from '../../services/food.service';

@Component({
  selector: 'app-food-list',
  templateUrl: './food-list.component.html',
  styleUrls: ['./food-list.component.scss'],
})
export class FoodListComponent implements OnInit {
  @Input() selectedFood?: Food;
  @Input() foods?: Food[];
  @Input() title?: string;

  constructor(public foodService: FoodService) { }

  ngOnInit() {}

  onSelect(food: Food): void {
    this.selectedFood = food;
  }

  checkbox(food: Food){
    console.log(food.name);
    this.foodService.selectedForBuy.push(food.name);
    this.foodService.saveData();
  }

}
